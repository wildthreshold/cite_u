<?php

namespace App\Controller;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CiteUController extends AbstractController
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

//    /**
//     * @Route("/change_locale/{locale}", name="change_locale")
//     */
//    public function changeLocale($locale, Request $request)
//    {
//        // On stocke la langue dans la session
//        $request->getSession()->set('_locale', $locale);
//
//        // On revient sur la page précédente
//        return $this->redirect($request->headers->get('referer'));
//    }

    
    /**
     * @Route("/", name="accueil")
     */
    public function index (): Response
    {
        return $this->render('cite_u/accueil.html.twig', [
          
        ]);
    }



    
    /**
     * @Route({
     *      "fr": "/genese",
     *      "en": "/origin"
     * }, name="genese")
     */
    public function genese (TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/genese_translations?filter[languages_code][_eq]=fr-FR&fields=*');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/genese_translations?filter[languages_code][_eq]=en-US&fields=*');
        }

        $content = $response->toArray()["data"][0];

        return $this->render('cite_u/genese.html.twig', [
//            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre" => $translator->trans('Genèse'),
            "bgImage" =>"build/images/bannieres/genese.jpg"
        ]);
    }




    /**
     * @Route({
     *      "fr": "/bureaux",
     *      "en": "/offices"
     * }, name="bureaux")
     */
    public function bureau (TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/bureaux_translations?filter[languages_code][_eq]=fr-FR&fields=*,images_interieurs.directus_files_id');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/bureaux_translations?filter[languages_code][_eq]=en-US&fields=*,images_interieurs.directus_files_id');
        }
        $content = $response->toArray()["data"][0];
        return $this->render('cite_u/bureau.html.twig', [
            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre" => $translator->trans('Bureaux'),
            "bgImage" =>"build/images/bannieres/bureaux.jpg"
        ]);
    }



    
    /**
     * @Route("/films-images", name="films-images")
     */
    public function imageFilm (TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/films_et_images_translations?filter[languages_code][_eq]=fr-FR&fields=*,images_interieures.directus_files_id,%20images_exterieures.directus_files_id');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/films_et_images_translations?filter[languages_code][_eq]=en-US&fields=*,images_interieures.directus_files_id,%20images_exterieures.directus_files_id');
        }
        $content = $response->toArray()["data"][0];

        return $this->render('cite_u/images-film.html.twig', [
            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre" => "Films & Images",
            "bgImage" =>"build/images/bannieres/images-films.jpg"
        ]);
    }



    
    /**
     * @Route({
     *      "fr": "/acces",
     *      "en": "/access"
     * }, name="acces")
     */
    public function acces (TranslatorInterface $translator): Response
    {
        return $this->render('cite_u/acces.html.twig', [
            "titre" => $translator->trans('Accès'),
            "bgImage" =>"build/images/bannieres/acces.jpg"
        ]);
    }




    /**
     * @Route({
     *      "fr": "/ecosysteme",
     *      "en": "/ecosystem"
     * }, name="ecosysteme")
     */
    public function ecosysteme (TranslatorInterface $translator, Request $request): Response
    {

        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/ecosysteme_translations?filter[languages_id][_eq]=fr-FR&fields=*,pole_complementaire_images.directus_files_id,pole_hospitality_images.directus_files_id,pole_sante_bien_etre_images.directus_files_id,pole_sportif_images.directus_files_id,pole_hospitality_images.directus_files_id,pole_sante_bien_etre_images.directus_files_id,pole_sportif_images.directus_files_id');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/ecosysteme_translations?filter[languages_id][_eq]=en-US&fields=*,pole_complementaire_images.directus_files_id,pole_hospitality_images.directus_files_id,pole_sante_bien_etre_images.directus_files_id,pole_sportif_images.directus_files_id,pole_hospitality_images.directus_files_id,pole_sante_bien_etre_images.directus_files_id,pole_sportif_images.directus_files_id');
        }

        $content = $response->toArray()["data"][0];

        return $this->render('cite_u/ecosysteme.html.twig', [
            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre"     => $translator->trans('Écosystème'),
            "bgImage"   => "build/images/bannieres/ecosysteme.jpg"
        ]);
    }


    

    /**
     * @Route({
     *      "fr": "/quartier",
     *      "en": "/neighbourhood"
     * }, name="quartier")
     */
    public function quartier (TranslatorInterface $translator): Response
    {
        return $this->render('cite_u/quartier.html.twig', [
            "titre" => $translator->trans('Quartier'),
            "bgImage" =>"build/images/bannieres/quartier.jpg"
        ]);
    }




    /**
     * @Route({
     *      "fr": "/cahier-technique",
     *      "en": "/technical-guide"
     * }, name="cahier-technique")
     */
    public function cahierTechnique (TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/cahier_technique_translations?filter[languages_code][_eq]=fr-FR');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/cahier_technique_translations?filter[languages_code][_eq]=en-US');
        }

        $content = $response->toArray()["data"][0];

        return $this->render('cite_u/cahier-technique.html.twig', [
//            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre" => $translator->trans('Cahier technique'),
            "bgImage" =>"build/images/bannieres/plans.jpg"
        ]);
    }




    /**
     * @Route({
     *      "fr": "/labels-certifications",
     *      "en": "/accreditions-certifications"
     * }, name="labels-certifications")
     */
    public function labelCertification (TranslatorInterface $translator): Response
    {
        return $this->render('cite_u/label-certification.html.twig', [
            "titre" => $translator->trans('Labels & certifications'),
            "bgImage" =>"build/images/bannieres/labels.jpg"
        ]);
    }


    

    /**
     * @Route({
     *      "fr": "/dates-cles",
     *      "en": "/key-dates"
     * }, name="dates-cles")
     */
    public function dateCles (TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/dates_cles_translations?filter[languages_code][_eq]=fr-FR');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/dates_cles_translations?filter[languages_code][_eq]=en-US');
        }
        $content = $response->toArray()["data"][0];
        return $this->render('cite_u/date-cles.html.twig', [
            "api_url"   => $_ENV["API_URL"],
            "content"   => $content,
            "titre" => $translator->trans('Dates clés'),
            "bgImage" =>"build/images/bannieres/dates.jpg"
        ]);
    }




    /**
     * @Route({
     *      "fr": "/mentions-legales",
     *      "en": "/legal-mentions"
     * }, name="mentions-legales")
     */
    public function mentionsLegales(TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/mentions_legales_translations?filter[languages_code][_eq]=fr-FR');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/mentions_legales_translations?filter[languages_code][_eq]=en-US');
        }
        $content = $response->toArray()["data"][0];
        return $this->render('cite_u/mentions.html.twig', [
            "content"   => $content,
            "titre" => $translator->trans('Mentions légales'),
            "bgImage" => "build/images/bannieres/dates.jpg"
        ]);
    }




    /**
     * @Route("/credits", name="credits")
     */
    public function credits(TranslatorInterface $translator, Request $request): Response
    {
        $current_lang = $request->getLocale();
        if ($current_lang == "fr") {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/credits_translations?filter[languages_code][_eq]=fr-FR');
        } else {
            $response = $this->client->request('GET', $_ENV["API_URL"] . 'items/credits_translations?filter[languages_code][_eq]=en-US');
        }
        $content = $response->toArray()["data"][0];
        return $this->render('cite_u/credits.html.twig', [
            "content"   => $content,
            "titre" => $translator->trans('Crédits'),
            "bgImage" => "build/images/bannieres/dates.jpg"
        ]);
    }


}
