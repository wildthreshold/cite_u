<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

use App\Form\ContactType;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(TranslatorInterface $translator, Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $contactFormData = $form->getData();

            $message = (new Email())
                ->from($contactFormData['email'])
                ->to('cite.universelle.paris@gmail.com')
                ->cc("n.chapuis@ga.fr")
                ->subject($contactFormData["sujet"])
                ->text($translator->trans('Envoyé par : ') . $contactFormData['email'].\PHP_EOL. $contactFormData['message'], 'text/plain');
            $mailer->send($message);
            $this->addFlash('success', $translator->trans('Votre message a bien été envoyé'));

            return $this->redirectToRoute('contact');
        }

        return $this->renderForm('cite_u/contact.html.twig', [
            'form' => $form,
            "titre" => "Contact",
            "bgImage" =>"build/images/bannieres/contact.jpg"
        ]);
    }
}
